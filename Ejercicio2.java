import java.util.Scanner;
import java.util.Arrays;

public class Ejercicio2{
    public static void main(String args []){
        Scanner s = new Scanner(System.in);
        int tam1 = 0;
        int tam2 = 0;
        int [] valores1;
        int [] valores2;
        int [] arrayFinal;
        
        System.out.println("Ingrese el tamaño del primer arreglo");
        tam1 = s.nextInt();
        valores1 = new int[tam1];

        System.out.println("Ingrese el tamaño del segundo arreglo");
        tam2 = s.nextInt();
        valores2 = new int[tam2];

        s = new Scanner(System.in);

        System.out.println("Ingrese los elementos del primer arreglo");
        for(int i = 0; i < valores1.length; i++){
            System.out.println("Ingrese el elemento "+(i+1));
            valores1[i] = s.nextInt();
        }

        System.out.println("Ingrese los elementos del segundo arreglo");
        for(int i = 0; i < valores2.length; i++){
            System.out.println("Ingrese el elemento "+(i+1));
            valores2[i] = s.nextInt();
        }

        arrayFinal = new int[(valores1.length + valores2.length)];

        for(int i = 0; i < valores1.length; i++){
            arrayFinal[i] = valores1[i];
        }

        for(int i = valores1.length, j = 0; i < arrayFinal.length && j < valores2.length; i++, j++){
            arrayFinal[i] = valores2[j];
        }

        System.out.println("Los elementos del arreglo son: ");
        Arrays.sort(arrayFinal);
        for(int i = arrayFinal.length - 1; i >= 0; i--){
            System.out.println(arrayFinal[i]);
        }
        
    }
}